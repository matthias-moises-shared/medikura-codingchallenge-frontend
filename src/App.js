import Layout from './hoc/Layout'
import ScrabbleAssistant from './containers/ScrabbleAssistant'

import { makeStyles } from '@mui/styles'

const useStyles = makeStyles({
  App: {
    textAlign: 'center',
  }
})

const App = () => {
  const styles = useStyles()

  return (
    <div className={styles.App}>
      <Layout>
        <ScrabbleAssistant />
      </Layout>
    </div>
  )
}

export default App
