import TextField from '@mui/material/TextField'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import Box from '@mui/system/Box'
import { makeStyles } from '@mui/styles'

const useStyles = makeStyles(theme => ({
  textfield: {
    width: '500px',
    maxWidth: '80%'
  }
}))

const Search = ({ search, handleSearchTermChange, handleSubmit }) => {
  const styles = useStyles()

  return (
    <form onSubmit={handleSubmit}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <TextField className={styles.textfield} label="Enter some characters..." variant="outlined" value={search} onChange={handleSearchTermChange} inputProps={{ maxLength: 15 }} required />
          <br />
          <small style={{ color: 'lightgray' }}>(Max. 15 characters)</small>
        </Grid>
        <Grid item xs={12}>
          <Button type="submit" variant="contained">Lets Go!</Button>
        </Grid>
      </Grid>
      <Box sx={{ m: 5 }} />
    </form>
  )
}

export default Search