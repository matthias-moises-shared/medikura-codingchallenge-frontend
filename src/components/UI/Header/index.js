import headerImage from '../../../assets/images/header.png'
import { AppBar, Toolbar, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'

const useStyles = makeStyles({
  headerImg: {
    display: 'block',
    width: '100%',
    height: 'auto',
    borderTop: '1px solid lightgray',
    borderBottom: '1px solid lightgray',
    '@media (max-width: 800px)': {
      display: 'none'
    }
  }
})

const Header = () => {
  const styles = useStyles()

  return (
    <header>
      <AppBar position="static" color="primary">
        <Toolbar>
          <Typography variant="h2" component="div" sx={{ flexGrow: 1 }} style={{ padding: '10px 0' }}>
            Scrabble Assistant
          </Typography>
        </Toolbar>
      </AppBar>
      <img className={styles.headerImg} src={headerImage} alt="header" />
    </header>
  )
}

export default Header