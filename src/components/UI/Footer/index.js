import AppBar from "@mui/material/AppBar"
import Toolbar from "@mui/material/Toolbar"
import Typography from "@mui/material/Typography"
import { makeStyles } from '@mui/styles'

const useStyles = makeStyles({
  footer: {
    flexGrow: 1,
  },
  appbar: {
    alignItems: 'center',
  }
})

const Footer = () => {
  const styles = useStyles()
  const currYear = new Date().getFullYear()

  return (
    <footer className={styles.footer}>
      <AppBar position="static" color="primary" className={styles.appbar}>

        <Toolbar>
          <Typography variant="body1" color="inherit">
            &copy; {currYear}
          </Typography>
        </Toolbar>

      </AppBar>
    </footer>
  )
}

export default Footer